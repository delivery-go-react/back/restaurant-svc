package db

import (
	"fmt"
	"log"

	_ "github.com/GoogleCloudPlatform/cloudsql-proxy/proxy/dialers/postgres"
	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/config"
	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Handler struct {
	DB *gorm.DB
}

func Init(c config.Config) Handler {
	user := c.DB_USER
	password := c.DB_PASSWORD
	dbHost := c.DB_HOST
	databaseName := c.DB_NAME

	connString := fmt.Sprintf("host=%s port=5432 user=%s dbname=%s sslmode=disable password=%s", dbHost, user, databaseName, password)

	db, err := gorm.Open(postgres.New(postgres.Config{
		DSN: connString,
	}), &gorm.Config{})

	if err != nil {
		log.Fatalln(err)
	}

	db.AutoMigrate(&models.Restaurant{})
	db.AutoMigrate(&models.Menu{})
	db.AutoMigrate(&models.Section{})
	db.AutoMigrate(&models.Position{})

	return Handler{db}
}
