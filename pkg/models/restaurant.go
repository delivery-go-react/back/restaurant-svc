package models

type Restaurant struct {
	ID          int64  `gorm:"primaryKey"`
	Name        string `gorm:"not null"`
	Description string `gorm:"not null"`
	Address     string `gorm:"not null"`
	Phone       string `gorm:"not null"`
	Email       string `gorm:"not null"`
	Menus       []Menu `gorm:"foreignKey:RestaurantID"`
}

type Menu struct {
	ID           int64     `gorm:"primaryKey"`
	Name         string    `gorm:"not null"`
	Description  string    `gorm:"not null"`
	RestaurantID int64     `gorm:"not null"`
	Sections     []Section `gorm:"foreignKey:MenuID"`
}

type Section struct {
	ID     int64      `gorm:"primaryKey"`
	Name   string     `gorm:"not null"`
	MenuID int64      `gorm:"not null"`
	Items  []Position `gorm:"foreignKey:SectionID"`
}

type Position struct {
	ID        int64   `gorm:"primaryKey"`
	Name      string  `gorm:"not null"`
	Price     float64 `gorm:"not null"`
	Image     string  `gorm:"not null"`
	SectionID int64   `gorm:"not null"`
	Section   Section `gorm:"foreignKey:SectionID"`
}
