package utils

import (
	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/models"
	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/pb"
)

func ToPBSection(section models.Section) *pb.Section {
	pbPositions := []*pb.Position{}
	for _, position := range section.Items {
		pbPosition := &pb.Position{
			Id:        position.ID,
			Name:      position.Name,
			Price:     float32(position.Price),
			Image:     position.Image,
			SectionId: position.SectionID,
		}
		pbPositions = append(pbPositions, pbPosition)
	}

	return &pb.Section{
		Id:        section.ID,
		Name:      section.Name,
		MenuId:    section.MenuID,
		Positions: pbPositions,
	}
}
