package utils

import (
	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/models"
	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/pb"
)

func ToPBRestaurant(r *models.Restaurant) *pb.Restaurant {
	pbMenus := make([]*pb.Menu, 0, len(r.Menus))
	for _, m := range r.Menus {
		pbSections := make([]*pb.Section, 0, len(m.Sections))
		for _, s := range m.Sections {
			pbPositions := make([]*pb.Position, 0, len(s.Items))
			for _, p := range s.Items {
				pbPositions = append(pbPositions, &pb.Position{
					Id:        p.ID,
					Name:      p.Name,
					Price:     float32(p.Price),
					Image:     p.Image,
					SectionId: p.SectionID,
				})
			}
			pbSections = append(pbSections, &pb.Section{
				Id:        s.ID,
				Name:      s.Name,
				MenuId:    s.MenuID,
				Positions: pbPositions,
			})
		}
		pbMenus = append(pbMenus, &pb.Menu{
			Id:           m.ID,
			Name:         m.Name,
			Description:  m.Description,
			RestaurantId: m.RestaurantID,
			Sections:     pbSections,
		})
	}
	return &pb.Restaurant{
		Id:          r.ID,
		Name:        r.Name,
		Description: r.Description,
		Address:     r.Address,
		Phone:       r.Phone,
		Email:       r.Email,
		Menus:       pbMenus,
	}
}
