package utils

import (
	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/models"
	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/pb"
)

func ToPBMenu(menu models.Menu) *pb.Menu {
	pbSections := []*pb.Section{}
	for _, section := range menu.Sections {
		pbItems := []*pb.Position{}
		for _, item := range section.Items {
			pbItem := &pb.Position{
				Id:        item.ID,
				Name:      item.Name,
				Price:     float32(item.Price),
				Image:     item.Image,
				SectionId: item.SectionID,
			}
			pbItems = append(pbItems, pbItem)
		}
		pbSection := &pb.Section{
			Id:        section.ID,
			Name:      section.Name,
			MenuId:    section.MenuID,
			Positions: pbItems,
		}
		pbSections = append(pbSections, pbSection)
	}

	return &pb.Menu{
		Id:           menu.ID,
		Name:         menu.Name,
		Description:  menu.Description,
		RestaurantId: menu.RestaurantID,
		Sections:     pbSections,
	}
}
