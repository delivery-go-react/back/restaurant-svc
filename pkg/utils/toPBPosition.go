package utils

import (
	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/models"
	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/pb"
)

func ToPBPosition(position *models.Position) *pb.Position {
	return &pb.Position{
		Id:        position.ID,
		Name:      position.Name,
		Price:     float32(position.Price),
		Image:     position.Image,
		SectionId: position.SectionID,
	}
}
