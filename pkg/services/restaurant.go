package services

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/db"
	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/models"
	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/pb"
	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/utils"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

type Server struct {
	H db.Handler
}

func (s *Server) GetRestaurants(ctx context.Context, req *pb.GetRestaurantsRequest) (*pb.GetRestaurantsResponse, error) {
	var restaurants []models.Restaurant
	var responseRestaurants []*pb.Restaurant

	if result := s.H.DB.Preload("Menus.Sections.Items").Find(&restaurants); result.Error != nil {
		return nil, result.Error
	}

	for _, restaurant := range restaurants {
		pbRestaurant := utils.ToPBRestaurant(&restaurant)
		responseRestaurants = append(responseRestaurants, pbRestaurant)
	}

	response := &pb.GetRestaurantsResponse{
		Restaurants: responseRestaurants,
	}

	return response, nil
}

func (s *Server) GetRestaurant(ctx context.Context, req *pb.GetRestaurantRequest) (*pb.GetRestaurantResponse, error) {
	var restaurant models.Restaurant
	if result := s.H.DB.Preload("Menus.Sections.Items").First(&restaurant, req.Id); result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, status.Errorf(codes.NotFound, "restaurant with id %d not found", req.Id)
		}
		return nil, status.Errorf(codes.Internal, "failed to get restaurant: %v", result.Error)
	}
	pbRestaurant := utils.ToPBRestaurant(&restaurant)
	return &pb.GetRestaurantResponse{Restaurant: pbRestaurant}, nil
}

func (s *Server) CreateRestaurant(ctx context.Context, req *pb.CreateRestaurantRequest) (*pb.CreateRestaurantResponse, error) {
	restaurant := &models.Restaurant{
		Name:        req.Name,
		Description: req.Description,
		Address:     req.Address,
		Phone:       req.Phone,
		Email:       req.Email,
	}

	if result := s.H.DB.Create(&restaurant); result.Error != nil {
		return nil, result.Error
	}

	pbRestaurant := utils.ToPBRestaurant(restaurant)

	response := &pb.CreateRestaurantResponse{
		Restaurant: pbRestaurant,
	}

	return response, nil
}

func (s *Server) UpdateRestaurant(ctx context.Context, req *pb.UpdateRestaurantRequest) (*pb.UpdateRestaurantResponse, error) {
	var restaurant models.Restaurant
	fmt.Println(req)

	if result := s.H.DB.First(&restaurant, req.Id); result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, status.Errorf(codes.NotFound, "restaurant with ID %d not found", req.Id)
		}
		return nil, result.Error
	}

	restaurant.Name = req.Name
	restaurant.Description = req.Description
	restaurant.Address = req.Address
	restaurant.Phone = req.Phone
	restaurant.Email = req.Email

	if result := s.H.DB.Save(&restaurant); result.Error != nil {
		return nil, result.Error
	}

	pbRestaurant := utils.ToPBRestaurant(&restaurant)

	response := &pb.UpdateRestaurantResponse{
		Restaurant: pbRestaurant,
	}

	return response, nil
}
