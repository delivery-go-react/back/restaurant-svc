package services

import (
	"context"
	"net/http"

	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/models"
	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/pb"
	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/utils"
)

func (s *Server) CreatePosition(ctx context.Context, req *pb.CreatePositionRequest) (*pb.CreatePositionResponse, error) {
	// Create a new Position model
	position := &models.Position{
		Name:      req.Name,
		Price:     float64(req.Price),
		Image:     req.Image,
		SectionID: req.SectionId,
	}

	// Insert the new position into the database
	if result := s.H.DB.Create(&position); result.Error != nil {
		return nil, result.Error
	}

	// Convert the Position model to a protobuf Position message
	pbPosition := utils.ToPBPosition(position)

	// Return a CreatePositionResponse message containing the new position
	response := &pb.CreatePositionResponse{
		Position: pbPosition,
	}
	return response, nil
}

func (s *Server) UpdatePosition(ctx context.Context, req *pb.UpdatePositionRequest) (*pb.UpdatePositionResponse, error) {
	// Find the position to update
	position := &models.Position{}
	if result := s.H.DB.First(&position, req.Id); result.Error != nil {
		return nil, result.Error
	}

	// Update the position model with the new values
	position.Name = req.Name
	position.Price = float64(req.Price)

	// Save the updated position to the database
	if result := s.H.DB.Save(&position); result.Error != nil {
		return nil, result.Error
	}

	// Convert the Position model to a protobuf Position message
	pbPosition := utils.ToPBPosition(position)

	// Return an UpdatePositionResponse message containing the updated position
	response := &pb.UpdatePositionResponse{
		Position: pbPosition,
	}
	return response, nil
}

func (s *Server) GetPosition(ctx context.Context, req *pb.GetPositionRequest) (*pb.GetPositionResponse, error) {
	// Find the position to get
	position := &models.Position{}
	if result := s.H.DB.First(&position, req.Id); result.Error != nil {
		return nil, result.Error
	}

	// Convert the Position model to a protobuf Position message
	pbPosition := utils.ToPBPosition(position)

	// Return a GetPositionResponse message containing the position
	response := &pb.GetPositionResponse{
		Position: pbPosition,
	}
	return response, nil
}

func (s *Server) DeletePosition(ctx context.Context, req *pb.DeletePositionRequest) (*pb.Response, error) {
	// Find the position to delete
	position := &models.Position{}
	if result := s.H.DB.First(&position, req.Id); result.Error != nil {
		return &pb.Response{Status: http.StatusNotFound}, result.Error
	}

	// Delete the position from the database
	if result := s.H.DB.Delete(&position); result.Error != nil {
		return &pb.Response{Status: http.StatusInternalServerError}, result.Error
	}

	// Return a success response
	response := &pb.Response{
		Status: http.StatusOK,
	}
	return response, nil
}
