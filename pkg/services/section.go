package services

import (
	"context"
	"errors"
	"net/http"

	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/models"
	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/pb"
	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/utils"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

// CreateSection creates a new section for a menu
func (s *Server) CreateSection(ctx context.Context, req *pb.CreateSectionRequest) (*pb.CreateSectionResponse, error) {
	menu := models.Menu{}
	if result := s.H.DB.Find(&menu, req.MenuId); result.Error != nil {
		return nil, status.Errorf(codes.NotFound, "Could not find menu with specified ID: %v", req.MenuId)
	}

	section := &models.Section{
		Name:   req.Name,
		MenuID: req.MenuId,
	}

	if err := s.H.DB.Create(section).Error; err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to create section of menu: %v", err)
	}

	menu.Sections = append(menu.Sections, *section)

	if err := s.H.DB.Save(&menu).Error; err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to update menus sections: %v", err)
	}

	pbSection := utils.ToPBSection(*section)

	response := &pb.CreateSectionResponse{
		Section: pbSection,
	}

	return response, nil
}

func (s *Server) GetSection(ctx context.Context, req *pb.GetSectionRequest) (*pb.GetSectionResponse, error) {
	var section models.Section
	if result := s.H.DB.Preload("Items").First(&section, req.Id); result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, status.Errorf(codes.NotFound, "section with id %d not found", req.Id)
		}
		return nil, status.Errorf(codes.Internal, "failed to get section: %v", result.Error)
	}
	pbSection := utils.ToPBSection(section)
	response := &pb.GetSectionResponse{
		Section: pbSection,
	}
	return response, nil
}

// UpdateSection updates an existing section
func (s *Server) UpdateSection(ctx context.Context, req *pb.UpdateSectionRequest) (*pb.UpdateSectionResponse, error) {
	section := models.Section{}
	if result := s.H.DB.First(&section, req.Id); result.Error != nil {
		return nil, result.Error
	}

	section.Name = req.Name

	if result := s.H.DB.Save(&section); result.Error != nil {
		return nil, result.Error
	}

	pbSection := utils.ToPBSection(section)

	response := &pb.UpdateSectionResponse{
		Section: pbSection,
	}

	return response, nil
}

// DeleteSection deletes an existing section and removes it from the related section
func (s *Server) DeleteSection(ctx context.Context, req *pb.DeleteSectionRequest) (*pb.Response, error) {
	// Find the section to delete
	section := &models.Section{}
	if result := s.H.DB.First(&section, req.Id); result.Error != nil {
		return &pb.Response{Status: http.StatusNotFound}, result.Error
	}

	// Delete the section from the database
	if result := s.H.DB.Delete(&section); result.Error != nil {
		return &pb.Response{Status: http.StatusInternalServerError}, result.Error
	}

	// Return a success response
	response := &pb.Response{
		Status: http.StatusOK,
	}
	return response, nil
}
