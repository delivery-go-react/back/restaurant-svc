package services

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/models"
	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/pb"
	"gitlab.com/delivery-go-react/back/restaurant-svc/pkg/utils"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

func (s *Server) CreateMenu(ctx context.Context, req *pb.CreateMenuRequest) (*pb.CreateMenuResponse, error) {
	restaurant := models.Restaurant{}
	if result := s.H.DB.Find(&restaurant, req.RestaurantId); result.Error != nil {
		return nil, status.Errorf(codes.NotFound, "Could not find restaurant with specified ID: %v", req.RestaurantId)
	}

	fmt.Println(req)

	menu := &models.Menu{
		Name:         req.Name,
		Description:  req.Description,
		RestaurantID: req.RestaurantId,
	}

	if err := s.H.DB.Create(menu).Error; err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to create menu: %v", err)
	}

	restaurant.Menus = append(restaurant.Menus, *menu)

	if err := s.H.DB.Save(&restaurant).Error; err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to update restaurant menus: %v", err)
	}

	pbMenu := utils.ToPBMenu(*menu)

	response := &pb.CreateMenuResponse{
		Menu: pbMenu,
	}

	return response, nil
}

func (s *Server) GetMenu(ctx context.Context, req *pb.GetMenuRequest) (*pb.GetMenuResponse, error) {
	var menu models.Menu
	if result := s.H.DB.Preload("Sections.Items").First(&menu, req.Id); result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, status.Errorf(codes.NotFound, "menu with id %d not found", req.Id)
		}
		return nil, status.Errorf(codes.Internal, "failed to get menu: %v", result.Error)
	}
	pbMenu := utils.ToPBMenu(menu)
	response := &pb.GetMenuResponse{
		Menu: pbMenu,
	}
	return response, nil
}

func (s *Server) UpdateMenu(ctx context.Context, req *pb.UpdateMenuRequest) (*pb.UpdateMenuResponse, error) {
	// Get menu by ID
	var menu models.Menu
	if result := s.H.DB.First(&menu, req.Id); result.Error != nil {
		return nil, status.Errorf(codes.NotFound, "Menu not found with id: %d", req.Id)
	}

	// Update menu
	menu.Name = req.Name
	menu.Description = req.Description

	if result := s.H.DB.Save(&menu); result.Error != nil {
		return nil, result.Error
	}

	// Convert to protobuf message
	pbMenu := utils.ToPBMenu(menu)

	response := &pb.UpdateMenuResponse{
		Menu: pbMenu,
	}

	return response, nil
}

func (s *Server) DeleteMenu(ctx context.Context, req *pb.DeleteMenuRequest) (*pb.Response, error) {
	// Find the menu to delete
	menu := &models.Menu{}
	if result := s.H.DB.First(&menu, req.Id); result.Error != nil {
		return &pb.Response{Status: http.StatusNotFound}, result.Error
	}

	// Delete the menu from the database
	if result := s.H.DB.Delete(&menu); result.Error != nil {
		return &pb.Response{Status: http.StatusInternalServerError}, result.Error
	}

	// Return a success response
	response := &pb.Response{
		Status: http.StatusOK,
	}
	return response, nil
}
